{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Integration maps for ID13\n",
    "\n",
    "This project contains notebooks to create integration maps from the acquired data.\n",
    "\n",
    "## Before running anything:\n",
    "\n",
    "- Rename or copy `example_run_config.yml` in `run_config.yml`\n",
    "- Set the parameters in your new config `run_config.yml`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import yaml\n",
    "\n",
    "with open(\"run_config.yml\") as f:\n",
    "    config = yaml.safe_load(f)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Step 0: Average files for the calibration\n",
    "\n",
    "The aim of this step is to average the multiple file acquired during the calibration run to get a single `EDF` file that will be used for pyFAI calibration.\n",
    "\n",
    "- Open [0_average_for_calibration.ipynb](./0_average_for_calibration.ipynb), set the parameters and run it\n",
    "- (Optional: only if there is dark current data): Open [0bis_average_dark_for_calibration.ipynb](./0bis_average_dark_for_calibration.ipynb) and run it\n",
    "\n",
    "If this step succeded, you should now have a `EDF` file at the path set in the configuration. The file can be viewed by running the cell below"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import fabio\n",
    "from pyFAI.gui.jupyter import display\n",
    "\n",
    "img = fabio.open(\n",
    "    os.path.join(config[\"output_folder\"], config[\"average\"][\"output_file\"])\n",
    ")\n",
    "display(img.data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Step 1: Do the calibration\n",
    "\n",
    "The aim of this step is to determine the geometry of the set-up from the calibration image produced in Step 0. This geometry will be saved in a PONI file that will be needed for the integration.\n",
    "\n",
    "You have two methods at your disposal:\n",
    "### Use `pyFAI-calib2`\n",
    "[pyFAI-calib2](http://www.silx.org/doc/pyFAI/latest/man/pyFAI-calib2.html) is a graphical user interface for determining the geometry of a detector using a reference sample. Tutorial available at http://www.silx.org/doc/pyFAI/latest/usage/cookbook/calib-gui/index.html#cookbook-calibration-gui. \n",
    "\n",
    "### Use the notebook [1_calibration.ipynb](./1_calibration.ipynb)\n",
    "[1_calibration.ipynb](./1_calibration.ipynb) uses the same process as `pyFAI-calib2` calibration but in more \"manual\" way.\n",
    "\n",
    "Whatever the method used, you should get a PONI file that can be viewed by running the cell below"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import JSON\n",
    "from pyFAI.io.ponifile import PoniFile\n",
    "\n",
    "poni_path = os.path.join(\n",
    "    config[\"output_folder\"],\n",
    "    config[\"calibration\"][\"poni_file\"],\n",
    ")\n",
    "print(f\"The PONI file is expected to be found at {poni_path}\")\n",
    "\n",
    "poni = PoniFile()\n",
    "poni.read_from_file(poni_path)\n",
    "JSON(poni.as_dict())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the step above didn't work, make sure that the PONI file is present at the path where it is expected to be, especially if you did the calibration with `pyFAI-calib2`.\n",
    "\n",
    "## Step 2: Integrate the data\n",
    "\n",
    "Once you have a valid PONI file, it is time to integrate the data. For spatially-resolved data (map), use the notebook [2a_integration_map.ipynb](./2a_integration_map.ipynb). For time-resolved data (loopscan), use the notebook [2b_integration_loopscan.ipynb](./2b_integration_loopscan.ipynb)\n",
    "\n",
    "Know that the computation can be quite lengthy (one to several hours) so before running the whole process, double-check your configuration !\n",
    "\n",
    "At the end of the integration, you can display a plot of integration of the average image of the map/scan. If the integration is not satisfactory, you should check the calibration and redo Step 1 if needed *or* check that there was no problem with your source data. You can also check the plot of accumulated intensity by running the cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import h5py\n",
    "import matplotlib.pyplot as plt\n",
    "from matplotlib.colors import LogNorm\n",
    "\n",
    "with h5py.File(\n",
    "    os.path.join(\n",
    "        config[\"output_folder\"],\n",
    "        config[\"integration\"][\"output_file\"],\n",
    "    ),\n",
    "    \"r\",\n",
    ") as output_file:\n",
    "    acc = output_file[\"merged/integrate/accumulated_intensity/data\"][()]\n",
    "\n",
    "if len(acc.shape) == 2:\n",
    "    plt.imshow(acc, norm=LogNorm(), origin=\"lower\")\n",
    "else:\n",
    "    plt.plot(acc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Step 3: Linear XRPD pattern fitting with references\n",
    "\n",
    "The XRPD patterns obtained after Step 2 will be background subtracted and fitted with reference standard patterns."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import h5py\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "input_file = config[\"linear_xrpd_fit\"][\"input_uri\"].split(\"::\")[0]\n",
    "mapname = os.path.splitext(os.path.basename(input_file))[0]\n",
    "output_file = os.path.join(config[\"output_folder\"], mapname + \"_fit.h5\")\n",
    "\n",
    "\n",
    "def display_image_with_axes(im_data, axes=None):\n",
    "    extent = [axes[0][0], axes[0][1], axes[1][0], axes[1][1]]\n",
    "    plt.figure()\n",
    "    plt.imshow(im_data, origin=\"lower\", extent=extent)\n",
    "\n",
    "\n",
    "def display_image_in_actual_size(im_data):\n",
    "    dpi = 80\n",
    "    height, width = im_data.shape\n",
    "\n",
    "    # What size does the figure need to be in inches to fit the image?\n",
    "    figsize = width / float(dpi), height / float(dpi)\n",
    "\n",
    "    # Create a figure of the right size with one axes that takes up the full figure\n",
    "    fig = plt.figure(figsize=figsize)\n",
    "    ax = fig.add_axes([0, 0, 1, 1])\n",
    "\n",
    "    # Display the image.\n",
    "    ax.imshow(im_data, origin=\"lower\", aspect=\"equal\")\n",
    "\n",
    "\n",
    "with h5py.File(output_file, mode=\"r\") as f:\n",
    "    nxdata = f[f\"{mapname}/linear_fit/parameters\"]\n",
    "    attrs = nxdata.attrs\n",
    "    axes = [nxdata[axis][()] for axis in attrs.get(\"axes\", list())]\n",
    "    names = [attrs[\"signal\"]] + attrs[\"auxiliary_signals\"].tolist()\n",
    "    for name in names:\n",
    "        im_data = nxdata[name][()]\n",
    "        if axes:\n",
    "            display_image_with_axes(im_data, axes)\n",
    "        else:\n",
    "            display_image_in_actual_size(im_data)\n",
    "        plt.colorbar()\n",
    "        plt.title(name)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Final step: Download your data and results:\n",
    "\n",
    "1. Open the terminal and go to the working directory:\n",
    "```\n",
    "cd /data/visitor/hg159/id13\n",
    "```\n",
    "2. Transfer your data to the ESRF ftp server\n",
    "```\n",
    "source ftptools.sh && ftpshare your_data your_tmp_folder/\n",
    "```\n",
    "\n",
    "You can access your data at http://ftp.esrf.fr/tmp/."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python3 (juno)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
