from contextlib import ExitStack
from typing import Callable, Optional, Union
from enum import Flag

import numpy as np
from PyMca5.PyMcaMath.fitting.SpecfitFuns import snip1d

from ..io import nexus as nx
from ..utils import assert_uri_different_file
from .parallel_nxdata import chunked_execution

Diagnostics = Flag("Diagnostics", "per_pixel average")
DefaultDiagnostics = Diagnostics.average


def subtract_background_slice(
    data: np.ndarray,
    stackaxis: np.ndarray,
    method: Union[str, Callable[[np.ndarray], np.ndarray]] = "snip",
    width: int = 40,
    roi_min: Optional[int] = None,
    roi_max: Optional[int] = None,
    smoothing: int = 1,
    diagnostics: Diagnostics = DefaultDiagnostics,
    **_
) -> dict:
    """
    :param data: 3D data with last dimension the stack axis
    :param stackaxis: 1D
    :param method: expected to accept a stack of 1D patterns (last dimension is the stack axis)
    :param width: SNIP parameter
    :param roi_min: ROI along the stack axis
    :param roi_max: ROI along the stack axis
    :param smoothing: SNIP parameter
    :param diagnostics: specify the types of information to return in addition to the result
    :returns: result and diagnostics
    """
    if roi_min is None:
        roi_min = 0
    if roi_max is None:
        roi_max = len(stackaxis)
    nroi = roi_max - roi_min
    if nroi < 1:
        raise ValueError("The ROI for background subtraction has 0 points")
    background = np.full(data.shape, np.nan)
    if method == "snip":
        for i in range(data.shape[0]):
            background[i, :, roi_min:roi_max] = snip1d(
                data[i, :, roi_min:roi_max], width, smoothing
            )
    elif callable(method):
        for i in range(data.shape[0]):
            background[i, :, roi_min:roi_max] = method(data[i, :, roi_min:roi_max])
    else:
        raise ValueError("method", method)

    results = {"subtracted": data - background}
    diagnostics_per_pixel = bool(diagnostics & diagnostics.per_pixel)
    diagnostics_average = bool(diagnostics & diagnostics.average)
    if diagnostics_per_pixel:
        results["data"] = data
        results["background"] = background
    if diagnostics_average:
        results["data_sum"] = data.sum(axis=(0, 1))
        results["background_sum"] = background.sum(axis=(0, 1))
        results["nsum"] = data.shape[0] * data.shape[1]
    return results


def subtract_background(
    nxdata_uri_in: str, nxdata_uri_out: str, resources: Optional[dict] = None, **params
) -> None:
    """Apply a shape-preserving method on NXdata

    :param str nxdata_uri_in:
    :param str nxdata_uri_out:
    """
    assert_uri_different_file(nxdata_uri_in, nxdata_uri_out)
    shape, dtype, signal_uri = nx.nxdata_info(nxdata_uri_in)
    dataset_arguments = {"shape": shape, "dtype": dtype}
    axes = nx.nxdata_data_axes(nxdata_uri_in, add_missing=True)

    diagnostics = params.setdefault("diagnostics", DefaultDiagnostics)
    diagnostics_per_pixel = bool(diagnostics & diagnostics.per_pixel)
    diagnostics_average = bool(diagnostics & diagnostics.average)
    nsum = 0
    data_sum = background_sum = None

    with ExitStack() as cstack:
        h5fileout = None
        for results, index in chunked_execution(
            subtract_background_slice, params, nxdata_uri_in, resources=resources
        ):
            if h5fileout is None:
                if diagnostics_per_pixel:
                    ctx = nx.write_nxdata_context(
                        nxdata_uri_out,
                        dataset_arguments=dataset_arguments,
                        axes=axes,
                        signals={
                            "subtracted": None,
                            "data": signal_uri,
                            "background": None,
                        },
                        mark_default=True,
                    )
                    h5subtracted, _, h5bkg_per_pixel = cstack.enter_context(ctx)
                else:
                    ctx = nx.write_nxdata_context(
                        nxdata_uri_out,
                        dataset_arguments=dataset_arguments,
                        axes=axes,
                        signals={
                            "subtracted": None,
                        },
                        mark_default=True,
                    )
                    (h5subtracted,) = cstack.enter_context(ctx)
                h5fileout = h5subtracted.file
                subtracted_uri = h5fileout.filename + "::" + h5subtracted.parent.name
            h5subtracted[index] = results["subtracted"]
            if diagnostics_per_pixel:
                h5bkg_per_pixel[index] = results["background"]
            if diagnostics_average:
                if nsum == 0:
                    data_sum = results["data_sum"]
                    background_sum = results["background_sum"]
                    nsum = results["nsum"]
                else:
                    data_sum += results["data_sum"]
                    background_sum += results["background_sum"]
                    nsum += results["nsum"]
            h5fileout.flush()

    if nsum != 0:
        data_sum /= nsum
        background_sum /= nsum
        lastaxis = list(axes.keys())[-1]
        axes = {lastaxis: axes[lastaxis]}
        signals = {
            "subtracted": {"data": data_sum - background_sum},
            "data": {"data": data_sum},
            "background": {"data": background_sum},
        }
        with nx.write_nxdata_context(
            subtracted_uri + "_average", axes=axes, signals=signals, mark_default=False
        ) as _:
            pass
