from contextlib import ExitStack
from collections import OrderedDict
from typing import List, Optional, Tuple
from enum import Flag

import numpy as np
from scipy.interpolate import interp1d
from scipy.optimize import nnls
from PyMca5.PyMcaMath.linalg import lstsq

from ..io import nexus as nx
from ..utils import assert_uri_different_file
from .parallel_nxdata import chunked_execution

Diagnostics = Flag("Diagnostics", "per_pixel average uncertainties")
DefaultDiagnostics = Diagnostics.uncertainties | Diagnostics.average


def linfit(
    A: np.ndarray, b: np.ndarray, positive: bool = False, uncertainties: bool = True
) -> Tuple[np.ndarray, Optional[np.ndarray]]:
    """Solve `argmin_p || Ap - b ||_2` with optionally `px >= 0`.

    :param A: linear basis (nx x nphases)
    :param b: patterns to fit (nx x npatterns)
    :param positive: force positive coefficients when `True`
    :param uncertainties: calculate uncertainties or not
    :returns: fit parameters `px` and optionally their uncertainties (nphases x npatterns)
    """
    if positive:
        x = np.array([nnls(A, bi)[0] for bi in b.T]).T
        if uncertainties:
            x_std = np.full(x.shape, np.nan)
            return x, x_std
        else:
            return x, None
    else:
        result = lstsq(
            A,
            b,
            uncertainties=uncertainties,
            covariances=False,
            digested_output=False,
        )
        if uncertainties:
            return result
        else:
            return result[0], None


def linear_fit_slice(
    data: np.ndarray,
    reference_patterns: np.ndarray,
    reference_names: List[str],
    xmask: Optional[np.ndarray] = None,
    positive: bool = False,
    diagnostics: Diagnostics = DefaultDiagnostics,
    **_,
) -> dict:
    """
    :param data: 3D data to be fitted (n0 x n1 x nx)
    :param reference_patterns: linear fit basis (nphases x nx)
    :param reference_names: names of the the linear basis (nphases)
    :param xmask: mask the last data dimension (nx)
    :param positive: force positive coefficients when `True`
    :param diagnostics: specify the types of information to return in addition to the fitted parameters
    :returns: fitted parameters and diagnostics
    """
    odata = data
    shape = np.product(data.shape[:-1]), data.shape[-1]
    data = data.reshape(shape).T  # nx x npatterns (npatterns=n0*n1)
    reference_patterns = reference_patterns.T  # nx x nphases
    if xmask is not None:
        xmask = xmask & np.isfinite(data[:, 0])
        xoshape = data.shape
        data = data[xmask]
        reference_patterns = reference_patterns[xmask]

    if not reference_patterns.shape[0]:
        raise RuntimeError("No valid data for linear fitting")

    uncertainties = bool(diagnostics & Diagnostics.uncertainties)
    parameters, parameters_std = linfit(
        reference_patterns, data, uncertainties=uncertainties, positive=positive
    )  # nphases x npatterns

    fitresult = dict()
    results = {"fitresult": fitresult}
    pshape = odata.shape[:-1]
    if uncertainties:
        for name, p, p_std in zip(reference_names, parameters, parameters_std):
            fitresult[name] = p.reshape(pshape), p_std.reshape(pshape)
    else:
        for name, p in zip(reference_names, parameters):
            fitresult[name] = p.reshape(pshape), None

    if xmask is not None:
        fit = np.full(xoshape, np.nan, dtype=parameters.dtype)
        fit[xmask] = reference_patterns.dot(parameters)
    else:
        fit = reference_patterns.dot(parameters)  # nx x npatterns

    if diagnostics & diagnostics.average:
        results["fit_sum"] = fit.sum(axis=-1)
        results["data_sum"] = odata.sum(axis=(0, 1))
        results["residuals_sum"] = results["data_sum"] - results["fit_sum"]
        results["nsum"] = odata.shape[0] * odata.shape[1]

    fit = fit.T.reshape(odata.shape)  # ndim0 x ndim1 x ... x nx
    residuals = odata - fit
    if diagnostics & diagnostics.per_pixel:
        results["fit"] = fit
        results["residuals"] = residuals

    total_absolute_residual = np.nansum(np.abs(residuals), axis=-1)
    if uncertainties:
        total_absolute_residual_std = np.full(total_absolute_residual.shape, np.nan)
    else:
        total_absolute_residual_std = None
    fitresult["total_absolute_residual"] = (
        total_absolute_residual,
        total_absolute_residual_std,
    )

    return results


def parse_reference_patterns(xdata, reference_patterns):
    """Interpolate all base patterns on xdata"""
    xmask = np.full(xdata.size, True)
    array = list()
    names = list()
    for name, x, y in reference_patterns:
        f = interp1d(x, y, kind="cubic", bounds_error=False, fill_value=np.nan)
        y = f(xdata)
        xmask &= np.isfinite(y)
        array.append(y)
        names.append(name)
    return names, np.array(array), xmask


def linear_fit(
    nxdata_uri_in: str, nxdata_uri_out: str, resources: Optional[dict] = None, **params
) -> None:
    """Apply a shape-preserving method on NXdata"""
    assert_uri_different_file(nxdata_uri_in, nxdata_uri_out)

    shape, dtype, signal_uri = nx.nxdata_info(nxdata_uri_in)

    axes = nx.nxdata_data_axes(nxdata_uri_in, add_missing=True)
    axes_names = list(axes.keys())
    axes_data = list(axes.values())
    xname = axes_names.pop(-1)
    xdata = axes_data.pop(-1)

    res = parse_reference_patterns(xdata, params.pop("reference_patterns"))
    params["reference_names"], params["reference_patterns"], params["xmask"] = res
    reference_names = params["reference_names"]
    parameters_names = reference_names + ["total_absolute_residual"]

    img_axes = OrderedDict(zip(reversed(axes_names), reversed(axes_data)))

    diagnostics = params.setdefault("diagnostics", DefaultDiagnostics)
    diagnostics_uncertainies = bool(diagnostics & Diagnostics.uncertainties)
    fit_per_pixel = bool(diagnostics & diagnostics.per_pixel)
    fit_average = bool(diagnostics & diagnostics.average)
    nsum = 0
    fit_sum = data_sum = residuals_sum = None

    filename, process = nx.parse_uri(nxdata_uri_out)
    parameters_name = "parameters"
    if len(process) == 0:
        entry = "entry"
        process = "process"
    elif len(process) == 1:
        entry = process[0]
        process = "process"
    elif len(process) == 2:
        entry, process = process
    else:
        parameters_name = process[2]
        entry, process = process[:2]

    def nxdata_uri(name):
        return filename + f"::/{entry}/{process}/{name}"

    with ExitStack() as cstack:
        h5fileout = None
        for results, index in chunked_execution(
            linear_fit_slice, params, nxdata_uri_in, resources=resources
        ):
            if h5fileout is None:
                # Linear coefficients of decomposition
                dataset_arguments = {"shape": shape[:-1], "dtype": float}
                ctx = nx.write_nxdata_context(
                    nxdata_uri(parameters_name),
                    dataset_arguments=dataset_arguments,
                    signals=parameters_names,
                    interpretation="image",
                    axes=img_axes,
                    mark_default=True,
                )
                h5parameters = dict(zip(parameters_names, cstack.enter_context(ctx)))

                # Linear coefficients of decomposition (errors)
                if diagnostics_uncertainies:
                    dataset_arguments = {"shape": shape[:-1], "dtype": float}
                    ctx = nx.write_nxdata_context(
                        nxdata_uri(parameters_name + "_errors"),
                        dataset_arguments=dataset_arguments,
                        signals=parameters_names,
                        interpretation="image",
                        axes=img_axes,
                        mark_default=False,
                    )
                    h5uncertainties = dict(
                        zip(parameters_names, cstack.enter_context(ctx))
                    )

                # Fit and residuals
                if fit_per_pixel:
                    dataset_arguments = {"shape": shape, "dtype": dtype}
                    signals = {"data": signal_uri, "fit": None, "residuals": None}
                    ctx = nx.write_nxdata_context(
                        nxdata_uri("fit"),
                        dataset_arguments=dataset_arguments,
                        axes=axes,
                        signals=signals,
                        mark_default=False,
                    )
                    _, h5fit, h5residuals = cstack.enter_context(ctx)

                # Base patterns for linear decomposition
                signals = {
                    name: {"data": pattern}
                    for name, pattern in zip(
                        reference_names, params["reference_patterns"]
                    )
                }
                ctx = nx.write_nxdata_context(
                    nxdata_uri("reference_patterns"),
                    signals=signals,
                    axes={xname: xdata},
                    mark_default=False,
                )
                h5basepatterns = cstack.enter_context(ctx)
                h5fileout = h5basepatterns[0].file
                h5fileout.flush()

            fitresult = results["fitresult"]
            for p_name, (p, p_std) in fitresult.items():
                h5parameters[p_name][index[:-1]] = p
                if diagnostics_uncertainies:
                    h5uncertainties[p_name][index[:-1]] = p_std
            if fit_per_pixel:
                h5fit[index] = results["fit"]
                h5residuals[index] = results["residuals"]
            if fit_average:
                if nsum == 0:
                    fit_sum = results["fit_sum"]
                    data_sum = results["data_sum"]
                    residuals_sum = results["residuals_sum"]
                    nsum = results["nsum"]
                else:
                    fit_sum += results["fit_sum"]
                    data_sum += results["data_sum"]
                    residuals_sum += results["residuals_sum"]
                    nsum += results["nsum"]
            h5fileout.flush()

    if nsum != 0:
        data_sum /= nsum
        fit_sum /= nsum
        residuals_sum /= nsum

        axes = {xname: xdata}
        signals = {
            "data": {"data": data_sum},
            "fit": {"data": fit_sum},
            "residuals": {"data": residuals_sum},
        }
        with nx.write_nxdata_context(
            nxdata_uri("fit_average"), axes=axes, signals=signals, mark_default=False
        ) as _:
            pass
