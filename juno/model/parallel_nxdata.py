"""Utilities to parallize processing of a single NXdata group.
"""

import sys
import numpy as np
import multiprocessing
from concurrent.futures import ProcessPoolExecutor
from typing import Callable, Iterator, Optional, Tuple, TypeVar

from ..io import nexus as nx

T = TypeVar("T")


def task_main(
    method_on_slice: Callable[..., T],
    method_params: dict,
    nxdata_uri: str,
    index: Tuple[slice, ...] = tuple(),
) -> T:
    """Read slice from NXdata and apply a method on it.

    :param callable method_on_slice: takes result from nxdata_slice
    """
    data, stackaxis = nx.nxdata_slice(nxdata_uri, index)
    return method_on_slice(data=data, stackaxis=stackaxis, **method_params)


def slice_range(n: int, chunk: int) -> Iterator[Tuple[int, int]]:
    nexpanded = (n + chunk - 1) // chunk * chunk
    for i in range(0, nexpanded, chunk):
        yield i, min(i + chunk, n)


def slice_generator(
    nxdata_uri: str, slice_max_mb: int = 50
) -> Iterator[Tuple[slice, ...]]:
    """Generate slice indices for an NXdata with a memory limit on the slice."""
    shape, dtype, _ = nx.nxdata_info(nxdata_uri)
    slice_axis = 0
    index = [slice(None) for _ in shape]

    tmp = list(shape)
    tmp[slice_axis] = 1
    slice_mb = np.product(tmp) * dtype.itemsize / 1024**2
    nslices_chunk = max(int(slice_max_mb / slice_mb), 1)
    nslices_total = shape[slice_axis]
    print("\nTotal number of slices = ", nslices_total)
    print("Number of slices per process = ", nslices_chunk)
    print("Slice size (MB) = ", slice_mb)
    print("Size per process (MB) = ", nslices_chunk * slice_mb)

    for start, stop in slice_range(nslices_total, nslices_chunk):
        index[slice_axis] = slice(start, stop)
        yield tuple(index)


def chunked_execution(
    method: Callable,
    method_params: dict,
    nxdata_uri: str,
    resources: Optional[dict] = None,
):
    """Parallelize a method on NXdata"""
    if resources is None:
        resources = dict()
    max_workers = resources.get("max_workers", 4)
    slice_max_mb = resources.get("slice_max_mb", 50)
    indices = list(slice_generator(nxdata_uri, slice_max_mb=slice_max_mb))
    ntasks = len(indices)
    if max_workers > 1 and ntasks > 1:
        # Forking caused HDF5 mixups (slices seem displaced, not sure what happens exactly)
        if sys.version_info < (3, 7):
            multiprocessing.set_start_method("spawn", force=True)
            executor_context = ProcessPoolExecutor(max_workers=max_workers)
        else:
            mp_context = multiprocessing.get_context("spawn")
            executor_context = ProcessPoolExecutor(
                max_workers=max_workers, mp_context=mp_context
            )
        with executor_context as executor:
            futures = list()
            for index in indices:
                f = executor.submit(
                    task_main,
                    method,
                    method_params,
                    nxdata_uri,
                    index=index,
                )
                futures.append((f, index))
            print(
                f"\nRun {ntasks} '{method.__name__}' tasks on {max_workers} workers ..."
            )
            for i, (f, index) in enumerate(futures):
                yield f.result(), index
                print(f"Saving results {int((i + 1)/ntasks*100)}%", end="\r")
    else:
        print(f"\nRun {len(indices)} '{method.__name__}' tasks locally ...")
        for i, index in enumerate(indices):
            result = task_main(method, method_params, nxdata_uri, index=index)
            yield result, index
            print(f"Saving results {int((i + 1)/ntasks*100)}%", end="\r")
