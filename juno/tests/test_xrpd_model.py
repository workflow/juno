from collections import OrderedDict
import numpy
import pytest
from silx.io import h5py_utils

import juno.io.nexus as nx
from juno.model.background import subtract_background
from juno.model.background import Diagnostics as BackgroundDiagnostics
from juno.model.linfit import linear_fit
from juno.model.linfit import Diagnostics as LinFitDiagnostics
from juno.io.xrpd_patterns import read_xrpd_patterns


def normgauss(x, p, s, a):
    return a / (s * numpy.sqrt(2 * numpy.pi)) * numpy.exp(-((x - p) ** 2) / (2 * s**2))


@pytest.fixture
def xrpd_pattern_fileformat(tmpdir):
    yield str(tmpdir / "base{}.dat")


@pytest.fixture
def xrpd_data_uri(tmpdir):
    yield str(tmpdir / "source.h5::/entry/pyfairesult/xrdmap")


@pytest.fixture
def xrpd_twotheta(xrpd_pattern_fileformat):
    yield numpy.linspace(5, 20, 333)


@pytest.fixture
def xrpd_patterns(xrpd_twotheta, xrpd_pattern_fileformat):
    params = [[8, 0.5], [11, 0.7], [16, 0.4]]
    patterns = numpy.vstack([normgauss(xrpd_twotheta, p, s, 1) for p, s in params])

    for i, values in enumerate(patterns):
        data = numpy.stack([xrpd_twotheta, values], axis=1)
        numpy.savetxt(xrpd_pattern_fileformat.format(i), data)

    yield patterns  # ncompounds x ntwotheta


@pytest.fixture
def xrpd_scale_maps():
    params = [[-4, 2, -4, 2], [-2, 4, -2, 4], [-2, 4, -4, 2]]
    maps = list()
    for ymin, ymax, xmin, xmax in params:
        y, x = numpy.mgrid[ymin:ymax:111j, xmin:xmax:222j]
        z = numpy.cos(x**2 + y**2) / 2 + 0.5  # from 0 to 1
        maps.append(z)
    yield numpy.dstack(maps)  # nrow x ncol x nbases


BACKGROUND = 10


@pytest.fixture
def xrpd_map(xrpd_data_uri, xrpd_twotheta, xrpd_patterns, xrpd_scale_maps):
    yield _xrpd_map(
        xrpd_data_uri,
        xrpd_twotheta,
        xrpd_patterns,
        xrpd_scale_maps,
        with_background=True,
    )


@pytest.fixture
def xrpd_map_wo_background(
    xrpd_data_uri, xrpd_twotheta, xrpd_patterns, xrpd_scale_maps
):
    yield _xrpd_map(
        xrpd_data_uri,
        xrpd_twotheta,
        xrpd_patterns,
        xrpd_scale_maps,
        with_background=False,
    )


def _xrpd_map(
    xrpd_data_uri, xrpd_twotheta, xrpd_patterns, xrpd_scale_maps, with_background=True
):
    axes = OrderedDict()
    axes["dim1"] = numpy.arange(xrpd_scale_maps.shape[1])
    axes["dim0"] = numpy.arange(xrpd_scale_maps.shape[0])
    axes["2theta"] = xrpd_twotheta
    data = xrpd_scale_maps.dot(xrpd_patterns)
    if with_background:
        dargs = {"data": data + BACKGROUND}
    else:
        dargs = {"data": data}
    with nx.write_nxdata_context(xrpd_data_uri, dataset_arguments=dargs, axes=axes):
        pass
    return data  # nrow x ncol x ntwotheta


RESOURCES = {"max_workers": 4, "slice_max_mb": 10}


def background_method(data):
    return numpy.full(data.shape, BACKGROUND)


@pytest.mark.parametrize(
    "method,diagnostics",
    [
        ["snip", BackgroundDiagnostics(0)],
        [background_method, BackgroundDiagnostics.per_pixel],
        [background_method, BackgroundDiagnostics.average],
        [
            background_method,
            BackgroundDiagnostics.per_pixel | BackgroundDiagnostics.average,
        ],
    ],
)
def test_xrpdmap_background(method, diagnostics, tmpdir, xrpd_map, xrpd_data_uri):
    out_uri = str(tmpdir / "process.h5")
    subtract_background(
        xrpd_data_uri,
        out_uri,
        method=method,
        diagnostics=diagnostics,
        resources=RESOURCES,
    )

    if method is background_method:
        atol = 1e-12
    else:
        atol = 1e-2
    per_pixel_uri = out_uri + "::/entry/process/data"
    with nx.read_nxdata_context(per_pixel_uri) as nxdata:
        auxiliary_signals = nxdata.attrs.get("auxiliary_signals", None)
        if diagnostics & BackgroundDiagnostics.per_pixel:
            assert set(auxiliary_signals) == {"data", "background"}
        else:
            assert auxiliary_signals is None
        assert nxdata.attrs["signal"] == "subtracted"
        calc = nxdata["subtracted"][()]
        expected = xrpd_map
        numpy.testing.assert_allclose(calc, expected, atol=atol)

    if diagnostics & BackgroundDiagnostics.average:
        average_uri = out_uri + "::/entry/process/data_average"
        with nx.read_nxdata_context(average_uri) as nxdata:
            average_calc = nxdata["subtracted"][()]
            expected = xrpd_map.mean(axis=(0, 1))
            numpy.testing.assert_allclose(average_calc, expected, atol=1e-12)
    else:
        with h5py_utils.File(out_uri, mode="r") as nxroot:
            with pytest.raises(KeyError):
                nxroot["/entry/process/data_average"]


@pytest.mark.parametrize(
    "positive,diagnostics",
    [
        [True, LinFitDiagnostics(0)],
        [False, LinFitDiagnostics.per_pixel],
        [
            False,
            LinFitDiagnostics.average | LinFitDiagnostics.uncertainties,
        ],
        [False, LinFitDiagnostics.per_pixel | LinFitDiagnostics.average],
    ],
)
def test_xrpdmap_linfit(
    positive,
    diagnostics,
    tmpdir,
    xrpd_map_wo_background,
    xrpd_data_uri,
    xrpd_pattern_fileformat,
    xrpd_scale_maps,
):
    pattern = xrpd_pattern_fileformat.format("*")
    reference_patterns = read_xrpd_patterns(pattern)
    names, _, _ = zip(*reference_patterns)
    assert sorted(names) == ["0", "1", "2"]

    out_uri = str(tmpdir / "process.h5")
    linear_fit(
        xrpd_data_uri,
        out_uri,
        reference_patterns=reference_patterns,
        positive=positive,
        diagnostics=diagnostics,
        resources=RESOURCES,
    )

    parameters_uri = out_uri + "::/entry/process/parameters"
    with nx.read_nxdata_context(parameters_uri) as nxdata:
        for compound in range(3):
            parameter = nxdata[str(compound)][()]
            expected = xrpd_scale_maps[..., compound]
            numpy.testing.assert_allclose(parameter, expected, atol=1e-12)
        assert nxdata["total_absolute_residual"].shape == parameter.shape

    if diagnostics & LinFitDiagnostics.uncertainties:
        uri = out_uri + "::/entry/process/parameters_errors"
        with nx.read_nxdata_context(uri) as nxdata:
            pass
    else:
        with h5py_utils.File(out_uri, mode="r") as nxroot:
            with pytest.raises(KeyError):
                nxroot["/entry/process/parameters_errors"]

    fit_per_pixel = LinFitDiagnostics.per_pixel
    if (diagnostics & fit_per_pixel) == fit_per_pixel:
        uri = out_uri + "::/entry/process/fit"
        with nx.read_nxdata_context(uri) as nxdata:
            calc = nxdata["fit"][()]
            expected = xrpd_map_wo_background
            numpy.testing.assert_allclose(calc, expected, atol=1e-12)
    else:
        with h5py_utils.File(out_uri, mode="r") as nxroot:
            with pytest.raises(KeyError):
                nxroot["/entry/process/fit"]

    fit_average = LinFitDiagnostics.average
    if (diagnostics & fit_average) == fit_average:
        uri = out_uri + "::/entry/process/fit_average"
        with nx.read_nxdata_context(uri) as nxdata:
            average_calc = nxdata["fit"][()]
            expected = xrpd_map_wo_background.mean(axis=(0, 1))
            numpy.testing.assert_allclose(average_calc, expected, atol=1e-12)
    else:
        with h5py_utils.File(out_uri, mode="r") as nxroot:
            with pytest.raises(KeyError):
                nxroot["/entry/process/fit_average"]
