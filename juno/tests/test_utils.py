import pytest
import h5py
from ..utils import get_number_unique, get_scans


def test_get_number_unique():
    # It should raise an error if the list is empty
    with pytest.raises(TypeError):
        assert get_number_unique([])

    # It should return the number of unique positions
    assert get_number_unique([4.5, 4.5, 4.6, 4.7, 4.7, 4.8]) == 4
    assert get_number_unique([0.0, 1.0, 0.0, 1.0]) == 2
    assert get_number_unique([-1.0, -2.0, -2.0, -2.2]) == 3

    # It should return the number of unique positions according to the tolerance
    assert get_number_unique([4.5, 4.5, 4.6, 4.7, 4.7, 4.8], tol=0.5) == 1
    assert get_number_unique([0.0, 1.0, 0.0, 1.0], tol=0.5) == 2
    assert get_number_unique([-1.0, -2.0, -2.0, -2.2], tol=0.5) == 2

    # Test with real values
    assert get_number_unique([7.707993, 7.708298]) == 1
    assert get_number_unique([47.062, 47.275]) == 2


def test_get_scans(tmp_path):
    with h5py.File(tmp_path / "test.h5", "w") as h5file:
        h5file.create_group("7.1")
        h5file.create_group("8.1")
        h5file.create_group("9.1")
        h5file.create_group("10.1")

        h5file.create_group("scan_11.1")
        h5file.create_group("scan_20.1")

        h5file.create_group("not_a_scan")

        retrieved_scan_names = [scan.name for scan in get_scans(h5file)]

    # Non-scan names should not be retrieved
    assert "/not_a_scan" not in retrieved_scan_names
    # Scans should be ordered by scan number
    assert retrieved_scan_names == [
        "/7.1",
        "/8.1",
        "/9.1",
        "/10.1",
        "/scan_11.1",
        "/scan_20.1",
    ]
