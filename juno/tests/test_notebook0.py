import os
import numpy as np
import h5py
import hdf5plugin
from testbook import testbook
from typing import List
import yaml

from .ABCTestNotebook import ABCTestNotebook
from ..io import nexus as nx
from .utils import get_frame_data, assert_is_numpy_array, is_file


# process should be Literal["dark", "average"] but Literal was only added in Python 3.8
def generate_notebook0_files(
    config_filename: str, process: str, n_files: int = 2, n_frames: int = 10
) -> List[str]:
    frame = get_frame_data()
    with open(config_filename) as f:
        config = yaml.safe_load(f)

    gen_dir = os.path.join(config["proposal_folder"], config[process]["directory"])
    os.makedirs(gen_dir, exist_ok=True)

    generated_files = [os.path.join(gen_dir, f"frame_{i}.h5") for i in range(n_files)]

    for filename in generated_files:
        with h5py.File(filename, "w") as h5file:
            entry_0000 = nx.create_entry(h5file, "entry_0000")
            measurement = nx.create_data_plot(entry_0000, "measurement", [], ["data"])
            data = np.empty((n_frames, *frame.shape))
            data[:] = frame
            measurement.create_dataset("data", data=data, **hdf5plugin.Bitshuffle())

    return generated_files


class TestNotebook0(ABCTestNotebook):
    def generate_files(self, config_filename: str) -> List[str]:
        return generate_notebook0_files(config_filename, "average")

    def test_notebook0(self):
        with testbook("0_average_for_calibration.ipynb", execute=True) as tb:
            assert_is_numpy_array(tb, "data")
            assert is_file(tb, "output_file")


class TestNotebook0bis(ABCTestNotebook):
    def generate_files(self, config_filename: str) -> List[str]:
        return generate_notebook0_files(config_filename, "dark")

    def test_notebook0bis(self):
        with testbook("0bis_average_dark_for_calibration.ipynb", execute=True) as tb:
            assert_is_numpy_array(tb, "data")
            assert is_file(tb, "output_file")
