import fabio.edfimage
import numpy as np
import os
from pyFAI.test.utilstest import UtilsTest
from testbook.client import TestbookNotebookClient
from typing import Tuple


def assert_is_numpy_array(tb: TestbookNotebookClient, array_name: str):
    """
    Checks that the given variable is an numpy array.
    This must be done in the notebook as the ndarray is not serializable
    """
    tb.inject(f"import numpy as np; assert isinstance({array_name}, np.ndarray);")


def is_file(tb: TestbookNotebookClient, filepath_ref: str):
    """
    Checks that the given variable holds a file path to a valid file.
    """
    return os.path.isfile(tb.ref(filepath_ref))


def get_frame_data() -> np.ndarray:
    return fabio.open(UtilsTest.getimage("Pilatus1M.edf")).data


def write_mask_file(mask_filepath: str, frame_shape: Tuple[int, int]):
    mask_data = np.ones(frame_shape, dtype=np.uint8)
    mask_data[:, frame_shape[1] // 2 :] = 1.0
    edf = fabio.edfimage.EdfImage(data=mask_data)
    edf.write(mask_filepath)


def write_poni_data(new_poni_path: str) -> str:
    original_poni = UtilsTest.getimage("Pilatus1M.poni")
    with open(original_poni, "r") as input:
        with open(new_poni_path, "w") as output:
            output.write(input.read())
            output.write("\nWavelength: 1.032222634095911e-10")

    return new_poni_path
