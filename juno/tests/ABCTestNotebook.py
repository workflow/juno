import os
import pytest
import yaml
from typing import List


class ABCTestNotebook:
    def generate_files(self, config_filename: str) -> List[str]:
        raise NotImplementedError(f"Config must be generated at {config_filename}")

    def generate_config_dict(self, data_dir: str):
        return {
            "proposal_folder": data_dir,
            "output_folder": os.path.join(data_dir, "output/output1"),
            "average": {
                "directory": "average",
                "output_file": "test.edf",
            },
            "dark": {
                "directory": "average",
                "output_file": "dark.edf",
            },
            "calibration": {
                "detector": "Pilatus1M",
                "calibrant": "AgBh",
                "wavelength": 1e-10,
                "poni_file": "test.poni",
            },
            "integration": {
                "input_file": "master_test.h5",
                "output_file": "integrated_test.h5",
                "separate_signals": False,
            },
            "linear_xrpd_fit": {
                "input_uri": "integrated_test.h5",
                "reference_patterns": os.path.join(data_dir, "references", "*.x_y"),
                "resources": {"max_workers": 4, "slice_max_mb": 50},
                "overwrite": False,
                "positive_constraints": True,
                "snip_width": 41,
                "roi_min": None,
                "roi_max": None,
                "fit_diagnostics": ["uncertainties", "average"],
                "background_diagnostics": "average",
            },
        }

    @pytest.fixture(scope="class")
    def config_generation(self, tmpdir_factory, request):
        config_filename = "run_config.yml"

        if os.path.isfile(config_filename):
            with open(config_filename) as f:
                original_config = f.read()
        else:
            original_config = None

        with open(config_filename, "w") as f:
            yaml.dump(self.generate_config_dict(str(tmpdir_factory.mktemp("test"))), f)

        def restore_config():
            if original_config is None:
                os.remove(config_filename)
            else:
                with open(config_filename, "w") as f:
                    f.write(original_config)

        request.addfinalizer(restore_config)

        return config_filename

    @pytest.fixture(scope="class", autouse=True)
    def gen_files_and_clean_up(self, config_generation, request):
        gen_files = self.generate_files(config_generation)

        def remove_files():
            for file in gen_files:
                os.remove(file)

        request.addfinalizer(remove_files)
