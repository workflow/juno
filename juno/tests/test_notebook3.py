import numpy as np
import os
from testbook import testbook
import yaml
from typing import List

from .ABCTestNotebook import ABCTestNotebook
from ..io import nexus as nx
from .utils import is_file


class TestNotebook3(ABCTestNotebook):
    def generate_files(self, config_filename: str) -> List[str]:
        with open(config_filename) as f:
            config = yaml.safe_load(f)
        os.makedirs(config["output_folder"], exist_ok=True)

        dargs = {"shape": (100, 101, 200), "dtype": float, "fillvalue": 10.0}

        input_uri = os.path.join(
            config["output_folder"], config["linear_xrpd_fit"]["input_uri"]
        )
        with nx.write_nxdata_context(input_uri, dataset_arguments=dargs) as datasets:
            gen_files = [datasets[0].file.filename]

        reference_format = config["linear_xrpd_fit"]["reference_patterns"]
        os.makedirs(os.path.dirname(reference_format), exist_ok=True)
        reference_format = reference_format.replace("*", "{}")
        x = np.arange(200)
        for iref in range(2):
            y = np.random.random(200)
            ref_data = np.stack([x, y], axis=1)
            filename = reference_format.format("compound" + str(iref))
            np.savetxt(filename, ref_data)
            gen_files.append(filename)

        return gen_files

    def test_notebook3(self):
        with testbook("3_linear_xrpd_fit.ipynb", execute=True) as tb:
            assert is_file(tb, "outfile")
