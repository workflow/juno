from typing import List
import numpy as np
import h5py
from testbook import testbook

from .. import VERSION
from .ABCTestNotebook import ABCTestNotebook
from .test_notebook2a import generate_notebook2_files
from .utils import (
    assert_is_numpy_array,
    is_file,
)


class TestNotebook2b(ABCTestNotebook):
    def generate_files(self, config_filename: str) -> List[str]:
        return generate_notebook2_files(config_filename, scan_shape=(10,))

    def test_notebook2(self):
        with testbook("2b_integration_loopscan.ipynb", execute=True, timeout=600) as tb:
            assert_is_numpy_array(tb, "mean_intensity")
            assert_is_numpy_array(tb, "acc_data")
            assert is_file(tb, "output_filename")

            source_file = tb.ref("source_filename")
            with h5py.File(source_file, "r") as h5file:
                mean_image = h5file["mean_image"][()]

            output_file = tb.ref("output_filename")
            with h5py.File(output_file, "r") as h5file:
                assert h5file.attrs["creator"] == "juno"
                assert h5file.attrs["creator_version"] == VERSION

                processed_mean_image = h5file["merged/average/image/data"][()]

            np.testing.assert_allclose(mean_image, processed_mean_image)


class TestNotebook2bIncomplete(TestNotebook2b):
    def generate_files(self, config_filename: str) -> List[str]:
        return generate_notebook2_files(
            config_filename, scan_shape=(10,), last_scan_incomplete=True
        )


class TestNotebook2bNonMapScan(TestNotebook2b):
    def generate_files(self, config_filename: str) -> List[str]:
        return generate_notebook2_files(
            config_filename, scan_shape=(10,), add_arbitrary_scan=True
        )


class TestNotebook2bWithMask(TestNotebook2b):
    def generate_config_dict(self, data_dir: str):
        config_dict = super().generate_config_dict(data_dir)
        config_dict["integration"]["mask_file"] = "mask.edf"

        return config_dict
