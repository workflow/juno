from functools import reduce
import os
from typing import List, Tuple, Union
import numpy as np
import pytest
from pyFAI.opencl import pyopencl
import h5py
import hdf5plugin
import yaml
from testbook import testbook

from .. import VERSION
from .ABCTestNotebook import ABCTestNotebook
from ..io import nexus as nx
from .utils import (
    assert_is_numpy_array,
    get_frame_data,
    is_file,
    write_mask_file,
    write_poni_data,
)


def generate_notebook2_files(
    config_filename: str,
    scan_shape: Union[Tuple[int, int], Tuple[int]] = (5, 6),
    mapX: int = 2,
    mapY: int = 1,
    last_scan_incomplete: bool = False,
    add_arbitrary_scan: bool = False,
    remove_scan: bool = False,
) -> List[str]:
    with open(config_filename) as f:
        config = yaml.safe_load(f)

    generated_files = []
    os.makedirs(config["output_folder"], exist_ok=True)
    new_poni = os.path.join(
        config["output_folder"],
        config["calibration"]["poni_file"],
    )
    write_poni_data(new_poni)
    generated_files.append(new_poni)

    frame = get_frame_data()

    gen_file = os.path.join(
        config["proposal_folder"], config["integration"]["input_file"]
    )
    generated_files.append(gen_file)

    if "mask_file" in config["integration"]:
        mask_file = os.path.join(
            config["output_folder"], config["integration"]["mask_file"]
        )
        write_mask_file(mask_file, frame.shape)
        generated_files.append(mask_file)

    with h5py.File(gen_file, "w") as h5file:
        nb_pixels = reduce(lambda v, acc: v * acc, scan_shape)
        scanIndex = 1
        summed_image = np.zeros_like(frame)
        n_tot_frames = 0
        for x in range(mapX):
            for y in range(mapY):
                is_last_scan = scanIndex == mapX * mapY

                # Remove last scan if asked
                if is_last_scan and remove_scan:
                    continue

                # Take only one third of the map for the last scan if asked
                n_frames = (
                    nb_pixels // 3
                    if is_last_scan and last_scan_incomplete
                    else nb_pixels
                )

                # Create data arrays
                scan = nx.create_entry(h5file, f"{scanIndex}.1")
                measurement = scan.create_group("measurement")
                data = np.array(
                    frame * np.random.random((n_frames, 1, 1)), dtype=frame.dtype
                )
                for data_frame in data:
                    summed_image += data_frame
                n_tot_frames += n_frames
                measurement.create_dataset(
                    "eiger",
                    data=data,
                    **hdf5plugin.Bitshuffle(),
                )
                measurement.create_dataset(
                    "ct24", data=100 * np.random.random((n_frames))
                )

                # Map
                if len(scan_shape) == 2:
                    create_aux_map_arrays(scan, scan_shape, measurement, x, y, n_frames)
                # Loopscan
                else:
                    scan.create_dataset(
                        "title",
                        data=nx.as_nxchar(f"loopscan {nb_pixels} 0.1"),
                    )
                    time = np.linspace(
                        0, 0.1 * nb_pixels, num=nb_pixels, endpoint=False
                    )
                    measurement.create_dataset(
                        "elapsed_time",
                        data=time[:n_frames],
                        **hdf5plugin.Bitshuffle(),
                    )

                scanIndex += 1

        if add_arbitrary_scan:
            scan = nx.create_entry(h5file, f"{scanIndex}.1")
            scan.create_dataset(
                "title",
                data=nx.as_nxchar("dscan usapy -0.05 0.05 50 0.1"),
            )

        h5file["mean_image"] = summed_image / n_tot_frames

    return generated_files


def create_aux_map_arrays(
    scan: h5py.Group,
    scan_shape: Tuple[int, int],
    measurement: h5py.Group,
    x: int,
    y: int,
    n_frames: int,
):
    nb_pixels = reduce(lambda v, acc: v * acc, scan_shape)
    scanX, scanY = scan_shape
    yStart, yEnd = y, y + 1
    xStart, xEnd = x, x + 1
    # yMotor, yStart, yEnd, yPixels, xMotor, xStart, xEnd, xPixels, dt
    scan.create_dataset(
        "title",
        data=nx.as_nxchar(
            f"akmap_lut( ustry, {yStart}, {yEnd}, {scanY}, ustrz, {xStart}, {xEnd}, {scanX}, 0.1 )"
        ),
    )

    motor_y = np.empty((scanX, scanY))
    motor_y[:, :] = np.linspace(yStart, yEnd, num=scanY, endpoint=False)
    measurement.create_dataset(
        "ustry_position",
        data=motor_y.reshape((nb_pixels))[:n_frames],
        **hdf5plugin.Bitshuffle(),
    )

    motor_x = np.empty((scanY, scanX))
    motor_x[:, :] = np.linspace(xStart, xEnd, num=scanX, endpoint=False)
    measurement.create_dataset(
        "ustrz",
        data=motor_x.T.reshape((nb_pixels))[:n_frames],
        **hdf5plugin.Bitshuffle(),
    )


class TestNotebook2a(ABCTestNotebook):
    def generate_files(self, config_filename: str) -> List[str]:
        return generate_notebook2_files(config_filename)

    def test_notebook2(self):
        with testbook("2a_integration_map.ipynb", execute=True, timeout=600) as tb:
            assert_is_numpy_array(tb, "mean_intensity")
            assert_is_numpy_array(tb, "acc_data")
            assert is_file(tb, "output_filename")

            source_file = tb.ref("source_filename")
            with h5py.File(source_file, "r") as h5file:
                mean_image = h5file["mean_image"][()]

            output_file = tb.ref("output_filename")
            with h5py.File(output_file, "r") as h5file:
                assert h5file.attrs["creator"] == "juno"
                assert h5file.attrs["creator_version"] == VERSION

                processed_mean_image = h5file["merged/average/image/data"][()]

            np.testing.assert_allclose(mean_image, processed_mean_image)


class TestNotebook2aIncomplete(TestNotebook2a):
    def generate_files(self, config_filename: str) -> List[str]:
        return generate_notebook2_files(config_filename, last_scan_incomplete=True)


class TestNotebook2aNonMapScan(TestNotebook2a):
    def generate_files(self, config_filename: str) -> List[str]:
        return generate_notebook2_files(config_filename, add_arbitrary_scan=True)


class TestNotebook2aWithMask(TestNotebook2a):
    def generate_config_dict(self, data_dir: str):
        config_dict = super().generate_config_dict(data_dir)
        config_dict["integration"]["mask_file"] = "mask.edf"

        return config_dict


class TestNotebook2aWithMissingScan(TestNotebook2a):
    def generate_files(self, config_filename: str) -> List[str]:
        return generate_notebook2_files(
            config_filename, mapX=2, mapY=2, remove_scan=True
        )

    def generate_config_dict(self, data_dir: str):
        config_dict = super().generate_config_dict(data_dir)
        config_dict["integration"]["allow_missing_patches"] = True

        return config_dict


@pytest.mark.skipif(pyopencl is None, reason="pyopencl is not installed")
class TestNotebook2aWithSignalSeparation(TestNotebook2a):
    def generate_config_dict(self, data_dir: str):
        config_dict = super().generate_config_dict(data_dir)
        config_dict["separate_signals"] = True

        return config_dict


@pytest.mark.skipif(pyopencl is None, reason="pyopencl is not installed")
class TestNotebook2aWithSignalSeparationAndMask(TestNotebook2a):
    def generate_config_dict(self, data_dir: str):
        config_dict = super().generate_config_dict(data_dir)
        config_dict["integration"]["mask_file"] = "mask.edf"
        config_dict["separate_signals"] = True

        return config_dict
