import os
import numpy as np
import pytest
import fabio
import psutil
from ..io.edf import convert_to_edf

NB_XAXIS = 5
NB_ROWS = 50
NB_COLUMNS = 100


def test_raises_error_1d(tmp_path):
    profile_axis_data = np.arange(NB_XAXIS)
    profile_data = np.random.random((NB_XAXIS))
    filename = str(tmp_path / "test.edf")

    with pytest.raises(ValueError):
        convert_to_edf(profile_axis_data, profile_data, filename)


def test_raises_error_shape_conflict(tmp_path):
    profile_axis_data = np.arange(NB_XAXIS + 1)
    profile_data = np.random.random((NB_ROWS, NB_COLUMNS, NB_XAXIS))
    filename = str(tmp_path / "test.edf")

    with pytest.raises(ValueError):
        convert_to_edf(profile_axis_data, profile_data, filename)


def read_profiles_from_edf(filename):
    with fabio.open(filename) as f:
        assert f.getNbFrames() == 1
        assert f.dim1 == NB_XAXIS
        assert f.dim2 == NB_ROWS * NB_COLUMNS + 1
        assert f.header["xrdua_1d"] == "True"
        return f.data


def test_convert(tmp_path):
    profile_axis = np.arange(NB_XAXIS)
    profile_stack = np.random.random((NB_ROWS, NB_COLUMNS, NB_XAXIS))
    filename = str(tmp_path / "test.edf")

    convert_to_edf(profile_axis, profile_stack, filename)
    profiles_from_edf = read_profiles_from_edf(filename)

    axis_from_edf = profiles_from_edf[0]
    np.testing.assert_array_equal(profile_axis, axis_from_edf)

    stack_from_edf = profiles_from_edf[1:].reshape(profile_stack.shape)
    np.testing.assert_array_equal(profile_stack, stack_from_edf)


def test_filesize_limit(tmp_path):
    nbytes_single_io = 2**31 - 4096

    statvfs = os.statvfs(tmp_path)
    free_disk = statvfs.f_frsize * statvfs.f_bavail
    free_mem = psutil.virtual_memory().available
    if free_disk < 4 * nbytes_single_io:
        pytest.skip("not enough disk space for this test")
    if free_mem < 4 * nbytes_single_io:
        pytest.skip("no enough memory for this test")

    itemsize = 8
    n = int((nbytes_single_io / NB_XAXIS / itemsize) ** 0.5) + 1
    profile_axis = np.arange(NB_XAXIS)
    profile_stack = np.random.random((n, n, NB_XAXIS))
    filename = str(tmp_path / "test.edf")

    convert_to_edf(profile_axis, profile_stack, filename)

    nbytes = os.stat(filename).st_size
    expected = (profile_stack.size + NB_XAXIS) * profile_stack.itemsize + 512
    assert nbytes == expected
