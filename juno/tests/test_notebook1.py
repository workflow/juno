import fabio.edfimage
import os
from testbook import testbook
from typing import List
import yaml

from .ABCTestNotebook import ABCTestNotebook
from .utils import get_frame_data, is_file


class TestNotebook1(ABCTestNotebook):
    def generate_files(self, config_filename: str) -> List[str]:
        with open(config_filename) as f:
            config = yaml.safe_load(f)
        os.makedirs(config["output_folder"], exist_ok=True)
        gen_file = os.path.join(
            config["output_folder"], config["average"]["output_file"]
        )

        frame = get_frame_data()
        average_image = fabio.edfimage.EdfImage(data=frame, header={})
        average_image.write(gen_file)

        return [gen_file]

    def test_notebook1(self):
        with testbook("1_calibration.ipynb", execute=True) as tb:
            assert is_file(tb, "poni_path")
