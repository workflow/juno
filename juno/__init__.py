import os.path
import numpy as np
import h5py
import hdf5plugin  # noqa : F401
from typing import Sequence

with open(os.path.join(os.path.dirname(__file__), "VERSION"), "r") as version_file:
    VERSION = version_file.read()


def average_files(
    files_to_average: Sequence[str], average_method: str, dectris_format: bool = False
) -> np.ndarray:
    if len(files_to_average) == 0:
        raise ValueError("No files to average !")

    if dectris_format:
        data_path = "/entry/data/data"
    else:
        data_path = "/entry_0000/measurement/data"

    if average_method == "mean":
        summed_array = None
        nb_arrays = 0

        for filename in files_to_average:
            with h5py.File(filename, "r") as h5file:
                if data_path not in h5file:
                    print(f"Skipping {filename} since there is no data at {data_path}")
                    continue
                dset = h5file[data_path]
                dset_sum = np.sum(dset, axis=0, dtype=float)
                if summed_array is None:
                    summed_array = dset_sum
                else:
                    summed_array += dset_sum
                nb_arrays += len(dset)

        return summed_array / nb_arrays
    elif average_method == "max":
        max_array = None

        for filename in files_to_average:
            with h5py.File(filename, "r") as h5file:
                if data_path not in h5file:
                    print(f"Skipping {filename} since there is no data at {data_path}")
                    continue
                dset = h5file[data_path]
                dset_max = np.max(dset, axis=0)
                if max_array is None:
                    max_array = dset_max
                else:
                    np.maximum(dset_max, max_array, out=max_array)
                    max_array += dset_max

        return max_array
    else:
        raise ValueError(
            f'{average_method} is not a valid averaging method. Accepted values: "mean", "max"'
        )
