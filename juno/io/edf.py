import numpy as np
from fabio.edfimage import EdfFrame


def convert_to_edf(
    profile_axis: np.ndarray,
    profile_stack: np.ndarray,
    output_filename: str,
) -> None:
    """Save an n-D stack of 1D profiles as a 2D stack of 1D profiles in EDF format."""
    if profile_axis.ndim != 1:
        raise ValueError("Expected profile_axis_data to have 1 dimension.")
    if profile_stack.ndim < 2:
        raise ValueError("Expected profile_data to have at least 2 dimensions.")

    if profile_stack.shape[-1] != profile_axis.size:
        raise ValueError(
            "Expected profile_axis_data to have {} values.".format(
                profile_stack.shape[-1]
            )
        )

    last_axis = profile_stack.ndim - 1
    if profile_stack.ndim > 2:
        s = profile_stack.shape
        profile_stack = profile_stack.reshape((np.product(s[:last_axis]), s[last_axis]))
    profile_stack = np.vstack((profile_axis, profile_stack))

    # https://github.com/silx-kit/fabio/issues/464
    # edf = EdfImage(data=profile_stack, header={"xrdua_1d": True})
    # edf.write(output_filename)

    frame = EdfFrame(data=profile_stack, header={"xrdua_1d": True})
    frame._index = 0
    with open(output_filename, mode="wb") as outfile:
        outfile.write(frame.get_edf_block())
