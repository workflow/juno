# https://gitlab.esrf.fr/-/snippets/221

import os
import json
from typing import List, Optional, Sequence, Tuple, Union, Iterator
from contextlib import contextmanager
from collections import OrderedDict

import h5py
import numpy as np
from silx.io import h5py_utils

from ..utils import parse_uri


nxchar = h5py.special_dtype(vlen=str)


def as_nxchar(s: Union[str, Sequence[str]]) -> np.ndarray:
    return np.array(s, dtype=nxchar)


def save_configuration(
    parent: Union[h5py.Group, h5py.File], configdict: dict
) -> h5py.Group:
    # https://manual.nexusformat.org/classes/base_classes/NXnote.html#nxnote
    note = parent.create_group("configuration")
    note.attrs["NX_class"] = "NXnote"
    note["data"] = as_nxchar(json.dumps(configdict, indent=2))
    note["type"] = as_nxchar("json")
    return note


def create_entry(h5file: h5py.File, name: str) -> h5py.Group:
    # https://manual.nexusformat.org/classes/base_classes/NXentry.html
    entry = h5file.create_group(name)
    entry.attrs["NX_class"] = "NXentry"

    return entry


@contextmanager
def write_nxroot_context(uri: str, mode="a") -> Iterator[h5py.File]:
    filename, _ = parse_uri(uri)
    directory = os.path.dirname(filename)
    if directory:
        os.makedirs(directory, exist_ok=True)
    with h5py_utils.File(filename, mode=mode) as f:
        f.attrs["NX_class"] = "NXroot"
        yield f


@contextmanager
def write_nxentry_context(uri: str, **kw) -> Iterator[h5py.Group]:
    with write_nxroot_context(uri, **kw) as nxroot:
        _, h5path = parse_uri(uri)
        if h5path:
            entry_name = h5path[0]
        else:
            entry_name = "entry"
        if entry_name in nxroot:
            yield nxroot[entry_name]
        else:
            yield create_entry(nxroot, entry_name)


def create_process(parent: h5py.Group, name: str) -> h5py.Group:
    # https://manual.nexusformat.org/classes/base_classes/NXprocess.html
    process = parent.create_group(name)
    process.attrs["NX_class"] = "NXprocess"

    return process


@contextmanager
def write_nxprocess_context(uri: str, **kw) -> Iterator[h5py.Group]:
    _, h5path = parse_uri(uri)
    if len(h5path) > 1:
        process_name = h5path[1]
    else:
        process_name = "process"
    with write_nxentry_context(uri, **kw) as nxentry:
        if process_name in nxentry:
            yield nxentry[process_name]
        else:
            yield create_process(nxentry, process_name)


def create_data_plot(
    parent: h5py.Group,
    name: str,
    axes_names: Sequence[str],
    signal_names: Sequence[str],
) -> h5py.Group:
    # https://manual.nexusformat.org/classes/base_classes/NXdata.html
    plot = parent.create_group(name)
    attrs = {"NX_class": "NXdata", "signal": signal_names[0]}
    if len(signal_names) > 1:
        attrs["auxiliary_signals"] = as_nxchar(signal_names[1:])
    if axes_names:
        attrs["axes"] = as_nxchar(axes_names)
    plot.attrs.update(attrs)
    return plot


H5DatasetRef = Union[h5py.Dataset, h5py.ExternalLink, h5py.SoftLink]


@contextmanager
def write_nxdata_context(
    uri: str,
    dataset_arguments: Optional[dict] = None,
    axes: Optional[dict] = None,
    signals: Optional[Union[dict, list]] = None,
    interpretation: str = "spectrum",
    mark_default: bool = True,
    **kw,
) -> Iterator[List[H5DatasetRef]]:
    _, h5path = parse_uri(uri)
    if len(h5path) == 3:
        data_name = h5path[2]
    else:
        data_name = "data"
    if dataset_arguments is None:
        dataset_arguments = dict()
    if not signals:
        signals = {"data": None}
    elif not isinstance(signals, dict):
        signals = {s: None for s in signals}
    if axes is None:
        axes = dict()

    with write_nxprocess_context(uri, **kw) as nxprocess:
        if data_name in nxprocess:
            datasets = [nxprocess[data_name][s] for s in signals]
        else:
            nxdata = create_data_plot(
                nxprocess, data_name, tuple(axes.keys()), tuple(signals.keys())
            )

            if mark_default:
                mark_default_plot_recursive(nxdata)
            attrs = nxdata.attrs
            attrs["interpretation"] = interpretation

            datasets: List[H5DatasetRef] = list()
            for name, dsetinfo in signals.items():
                if isinstance(dsetinfo, str):
                    link = dsetinfo
                    if "::" in link:
                        filename, _, h5path = link.partition("::")
                        dset = h5py.ExternalLink(filename, h5path)
                    else:
                        dset = h5py.SoftLink(link)
                    nxdata[name] = dset
                elif isinstance(dsetinfo, dict):
                    dset = nxdata.create_dataset(
                        name, **{**dataset_arguments, **dsetinfo}
                    )
                else:
                    dset = nxdata.create_dataset(name, **dataset_arguments)
                datasets.append(dset)

            for name, values in axes.items():
                nxdata[name] = values
        yield datasets


@contextmanager
def read_nxdata_context(uri: str) -> Iterator[h5py.Group]:
    filename, h5path = parse_uri(uri)
    if len(h5path) > 1:
        entry_name = h5path[0]
        data_name = "/".join(h5path[1:])
    elif len(h5path) == 1:
        entry_name = h5path[0]
        data_name = None
    else:
        entry_name = None
        data_name = None
    with h5py_utils.File(filename, mode="r") as nxroot:
        if entry_name:
            parent = nxroot[entry_name]
        else:
            parent = nxroot
        if not data_name:
            data_name = parent.attrs.get("default", None)
            assert data_name, "No default NXdata found"
        yield parent[data_name]


def nxdata_info(uri: str, **kw) -> Tuple[tuple, np.dtype, str]:
    with read_nxdata_context(uri, **kw) as nxdata:
        signal = nxdata[nxdata.attrs["signal"]]
        assert isinstance(signal, h5py.Dataset)
        return signal.shape, signal.dtype, signal.file.filename + "::" + signal.name


Index = Union[slice, int]


def nxdata_slice(
    uri: str, index: Union[Index, Sequence[Index]]
) -> Tuple[np.ndarray, np.ndarray]:
    """Read axis and slice from data of NXdata"""
    with read_nxdata_context(uri) as nxdata:
        attrs = nxdata.attrs
        signal = nxdata[attrs["signal"]][index]
        axes = attrs.get("axes", None)
        if axes is None:
            axis = np.arange(signal.shape[-1])
        else:
            axis = nxdata[axes[-1]][()]
        return signal, axis


def nxdata_data_axes(uri: str, add_missing: Optional[bool] = False) -> OrderedDict:
    """Read all axes data from NXdata"""
    with read_nxdata_context(uri) as nxdata:
        attrs = nxdata.attrs
        data = OrderedDict()
        for name in attrs.get("axes", tuple()):
            data[name] = nxdata[name][()]
        if add_missing and not data:
            # NXdata convention: dim1, dim0, dim2, dim3, ...
            shape = list(nxdata[attrs["signal"]].shape)
            names = [f"dim{i}" for i in range(len(shape))]
            shape[1], shape[0] = shape[0], shape[1]
            names[1], names[0] = names[0], names[1]
            for name, n in zip(names, shape):
                data[name] = np.arange(n)
        return data


def mark_default_plot(plot: h5py.Group, entity: Union[h5py.File, h5py.Group]) -> None:
    # Remove first slash
    entity_path = entity.name[1:].split("/")
    plot_path = plot.name[1:].split("/")

    for folder in entity_path:
        if plot_path[0] == folder:
            plot_path.pop(0)
    entity.attrs["default"] = "/".join(plot_path)


def mark_default_plot_recursive(plot: h5py.Group, up_to: str = None) -> None:
    endgroup = up_to if up_to is not None else plot.file
    h5group = plot

    while h5group != endgroup and h5group != h5group.parent:
        new_h5group = h5group.parent
        mark_default_plot(plot, new_h5group)
        h5group = new_h5group


def add_dependencies(process: h5py.Group, dependencies: Sequence[str]) -> None:
    h5grp = process.create_group("dependencies")
    h5grp.attrs["NX_class"] = "NXcollection"
    for uri in dependencies:
        name = uri.split("/")[-1]
        filename, grpname = uri.split("::")
        h5grp[name] = h5py.ExternalLink(filename, grpname)


def create_pyFAI_process(
    entry: h5py.Group,
    pyFAI_version: str,
    pyFAI_config: dict,
    process_name: str = "integrate",
    dependencies: Sequence[str] = None,
) -> h5py.Group:
    pyFAI_process = create_process(entry, process_name)
    pyFAI_process.attrs.update({"program": "pyfai", "version": pyFAI_version})
    save_configuration(pyFAI_process, pyFAI_config)
    if dependencies is not None and len(dependencies) > 0:
        add_dependencies(pyFAI_process, dependencies)

    return pyFAI_process


def create_simple_pyFAI_plot(
    pyFAI_process: h5py.Group,
    radial_values: dict,
    radial_unit: str,
    integration: np.ndarray,
    data_group_name: str = "integration",
) -> h5py.Group:
    plot = create_data_plot(
        pyFAI_process,
        data_group_name,
        ["twotheta"],
        ["data"],
    )

    plot["data"] = integration
    plot["data"].attrs["interpretation"] = "spectrum"
    plot["twotheta"] = radial_values
    plot["twotheta"].attrs["units"] = radial_unit
    mark_default_plot(plot, pyFAI_process)

    return plot
