import os
import re
from glob import glob
from typing import List, Tuple, Union
import numpy as np


def read_xrpd_patterns(
    files: Union[str, tuple, list], extract_name_from_pattern: bool = True
) -> List[Tuple[str, np.ndarray, np.ndarray]]:
    if isinstance(files, str):
        pattern = os.path.basename(files)
        pattern, ext = os.path.splitext(pattern)
        pattern = pattern.replace("*", "(.*)")
        pattern = re.compile(pattern)
        files = glob(files)
    elif isinstance(files, (tuple, list)):
        pattern = None
    else:
        raise TypeError("Files must be either a search pattern or a list of strings")
    result = list()
    for filename in files:
        xi, yi = np.loadtxt(filename).T

        basename = os.path.basename(filename)
        basename, ext = os.path.splitext(basename)
        name = basename

        if extract_name_from_pattern and pattern:
            results = pattern.findall(basename)
            if results:
                results = results[0]
                if isinstance(results, tuple):
                    name = "_".join(results)
                else:
                    name = results

        result.append((name, xi, yi))
    return result
