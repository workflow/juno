import numpy as np
import re
from typing import List
import h5py

SATURATION_VALUE = (1 << 32) - 1  # 2^32 - 1

SCAN_REGEXP = re.compile(r".*?(\d+\.\d+)")


def extend_linear_dataset(data: np.ndarray, desired_length: int) -> np.ndarray:
    # Try to infer the final point from the linear relation y = a*n + y0
    a = (data[-1] - data[0]) / (len(data) - 1)
    y0 = data[0]
    final_point = a * desired_length + y0

    return np.linspace(y0, final_point, num=desired_length, endpoint=False)


def get_scans(h5file: h5py.File) -> List[h5py.Group]:
    scans = [
        child
        for child_name, child in h5file.items()
        # Scans are groups with names ending by 1.1, 2.1, ...
        if (isinstance(child, h5py.Group) and SCAN_REGEXP.match(child_name) is not None)
    ]

    return sorted(scans, key=scan_sort_key)


def hot_pixel_mask(data, max_value=SATURATION_VALUE):
    return data == max_value


def parse_uri(uri):
    filename, _, h5path = uri.partition("::")
    return filename, [v for v in h5path.split("/") if v]


def assert_uri_different_file(uri1, uri2):
    filename1, _, _ = uri1.partition("::")
    filename2, _, _ = uri2.partition("::")
    assert filename1 != filename2, "Do not use the same HDF5 file for input and output"


def get_number_unique(positions: List[float], tol: float = 1e-3) -> int:
    """
    Gets the number of unique values (motor positions) in a list according to a certain tolerance in O(n^2).
    Default tolerance is 1e-3 as motors positions are expected in mm and variations should be above the um-scale to be considered unique.

    :param positions: List containing the motor positions
    :param tol: Tolerance when checking the uniqueness. If abs(pos1 - pos0) > tol, pos0 and pos1 are considered unique.
    """
    if len(positions) == 0:
        raise TypeError(
            "Number of unique values can only be computed from a non-empty list"
        )

    # Always include the first position
    unique_positions = [positions[0]]
    for pos in positions[1:]:
        is_unique = all(
            [abs(pos - unique_pos) > tol for unique_pos in unique_positions]
        )
        if is_unique:
            unique_positions.append(pos)

    return len(unique_positions)


def get_scan_name(scan: h5py.Group) -> str:
    # scan.name is in fact the path to the entity
    scan_path = scan.name
    assert isinstance(scan_path, str)

    return scan_path.split("/")[-1]


def scan_sort_key(scan: h5py.Group) -> float:
    scan_name = get_scan_name(scan)
    match = SCAN_REGEXP.match(scan_name)
    assert match is not None

    return float(match.group(1))
