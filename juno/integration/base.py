import numpy as np
import h5py
import hdf5plugin  # noqa : F401
import tqdm
from typing import Any, Dict, Optional, Tuple
import pyFAI
from pyFAI.azimuthalIntegrator import AzimuthalIntegrator
from pyFAI.units import to_unit, Unit
from silx.io.utils import h5py_read_dataset

from .. import VERSION
from ..utils import get_scans, hot_pixel_mask
from ..io import nexus as nx
from .utils import Integrations, generate_dectris_frame_iterator


class BaseIntegrator:
    valid_scan_titles = []

    def __init__(
        self,
        source_file: str,
        output_file: str,
        ai: AzimuthalIntegrator,
        integration_params: dict,
        separation_params: Optional[Dict[str, Any]] = None,
        dectris_format: bool = False,
    ) -> None:
        self.source_file = source_file
        self.output_file = output_file
        self.ai = ai
        self.integration_params = integration_params
        self.dectris_format = dectris_format

        if separation_params is not None:
            from pyFAI.opencl import ocl
            from pyFAI.opencl.peak_finder import OCL_PeakFinder

            unit = to_unit(integration_params["unit"])
            assert isinstance(unit, Unit)

            mask = integration_params["mask"]
            mask = mask.astype("int8") if mask is not None else None

            integrator = ai.setup_CSR(
                ai.detector.shape,
                integration_params["npt"],
                mask=mask,
                unit=unit,
            )

            self.peak_finder = OCL_PeakFinder(
                integrator.lut,
                image_size=ai.detector.shape[0] * ai.detector.shape[1],
                unit=unit,
                bin_centers=integrator.bin_centers,
                radius=ai._cached_array[unit.name.split("_")[0] + "_center"],
                mask=mask,
                ctx=ocl.create_context(),
                empty=integration_params["dummy"],
            )

            self.integration_types = ["integration_background", "integration_peaks"]

            def integrate(frame, **integration_params):
                return self.integration_with_separation(
                    frame, separation_params, integration_params
                )

            self.integrate_frame = integrate
            self.separation_params = separation_params
        else:
            self.peak_finder = None
            self.integration_types = ["integration_map"]
            self.integrate_frame = self.simple_integration

    def integrate_file(self) -> int:
        N_frames = 0
        summed_image = None
        summed_integrations = {}

        # Flush prints to avoid interference with tqdm
        print(f"Processing {self.source_file}...", flush=True)

        with h5py.File(self.output_file, "a") as output:
            with h5py.File(self.source_file, "r") as h5file:
                # Ignore non-valid scans
                scans = [
                    scan
                    for scan in get_scans(h5file)
                    if any(
                        [
                            h5py_read_dataset(scan["title"]).startswith(
                                valid_scan_title
                            )
                            for valid_scan_title in self.valid_scan_titles
                        ]
                    )
                ]

                if len(scans) == 0:
                    raise TypeError(
                        f"Found no scan with title starting by {self.valid_scan_titles} !"
                    )

                for scan in scans:
                    scan_name = scan.name
                    print(
                        f"Integrating {scan_name} ({self.get_scan_size(scan)})...",
                        flush=True,
                    )
                    (
                        sum_scan_images,
                        sum_scan_integrations,
                        N_frames_in_scan,
                        radial_values,
                        radial_unit,
                        integrations,
                    ) = self.integrate_scan(scan)

                    summed_image = (
                        sum_scan_images
                        if summed_image is None
                        else summed_image + sum_scan_images
                    )
                    N_frames += N_frames_in_scan

                    scan_entry = nx.create_entry(output, scan_name)
                    pyFAI_process = nx.create_pyFAI_process(
                        scan_entry,
                        pyFAI.version,
                        self.ai.get_config(),
                        dependencies=[f"{self.source_file}::{scan_name}"],
                    )
                    scan_entry.create_dataset(
                        "normalization", data=self.get_normalization(scan)
                    )

                    for integ_type in self.integration_types:
                        summed_integrations[integ_type] = (
                            sum_scan_integrations[integ_type]
                            if integ_type not in summed_integrations
                            else summed_integrations[integ_type]
                            + sum_scan_integrations[integ_type]
                        )
                        self.create_pyFAI_plot(
                            pyFAI_process,
                            radial_values,
                            radial_unit,
                            integrations[integ_type],
                            scan,
                            integ_type,
                        )

                entry = nx.create_entry(output, "merged")
                avg_process = nx.create_pyFAI_process(
                    entry,
                    pyFAI.version,
                    self.ai.get_config(),
                    "average",
                    dependencies=[
                        f"{self.source_file}::{scan_name}" for scan_name in scans
                    ],
                )

                default_plot = self.create_average_plots(
                    avg_process,
                    summed_image,
                    summed_integrations,
                    N_frames,
                )
                nx.mark_default_plot(default_plot, entry)
                nx.mark_default_plot(entry, output)

                output.attrs.update({"creator": "juno", "creator_version": VERSION})

        self.merge_scans(self.output_file)

        return N_frames

    def integrate_scan(
        self, scan: h5py.Group
    ) -> Tuple[np.ndarray, Integrations, int, np.ndarray, str, Integrations]:
        scan_dataset = scan["measurement/eiger"]
        assert isinstance(scan_dataset, h5py.Dataset)

        integrations: Integrations = {
            k: np.zeros((self.get_scan_frames(scan), self.integration_params["npt"]))
            for k in self.integration_types
        }
        summed_frames = np.zeros_like(scan_dataset[0])
        assert isinstance(summed_frames, np.ndarray)

        if self.dectris_format:
            frame_iterator = generate_dectris_frame_iterator(scan_dataset)
        else:
            frame_iterator = scan_dataset

        for i, frame in enumerate(tqdm.tqdm(frame_iterator)):
            summed_frames += frame
            radial_values, unit, frame_integration = self.integrate_frame(
                frame, **self.integration_params
            )
            for k in self.integration_types:
                integrations[k][i] = frame_integration[k]

        summed_integrations = {
            k: np.sum(integration, axis=0) for k, integration in integrations.items()
        }

        return (
            summed_frames,
            summed_integrations,
            scan_dataset.shape[0],
            radial_values,
            unit,
            integrations,
        )

    def integration_with_separation(
        self,
        frame: np.ndarray,
        separation_params: Dict[str, Any],
        integration_params: Dict[str, Any],
    ) -> Tuple[np.ndarray, str, Integrations]:
        sparse_res = self.peak_finder.sparsify(
            frame, error_model="azimuthal", **separation_params
        )

        sclike = np.zeros_like(frame)
        flat = sclike.ravel()
        flat[sparse_res.index] = sparse_res.intensity
        res_peak = self.ai.integrate1d(sclike, **integration_params)

        return (
            res_peak.radial,
            res_peak.unit.unit_symbol,
            {
                "integration_background": sparse_res.background_avg,
                "integration_peaks": res_peak.intensity,
            },
        )

    def simple_integration(
        self, frame: np.ndarray, **integration_params
    ) -> Tuple[np.ndarray, str, Integrations]:
        integration = self.ai._integrate1d_ng(frame, **integration_params)

        return (
            integration.radial,
            integration.unit.unit_symbol,
            {"integration_map": integration.intensity},
        )

    def create_pyFAI_plot(
        self,
        pyFAI_process: h5py.Group,
        radial_values: np.ndarray,
        radial_unit: str,
        frame_integrations: np.ndarray,
        scan: h5py.Group,
        plot_name: str,
    ) -> h5py.Group:
        raise NotImplementedError()

    def get_scan_size(self, scan: h5py.Group):
        raise NotImplementedError()

    def get_scan_frames(self, scan: h5py.Group) -> int:
        raise NotImplementedError()

    def get_normalization(self, scan: h5py.Group) -> np.ndarray:
        nFrames = self.get_scan_frames(scan)
        if "measurement/ct24" not in scan:
            return np.ones(nFrames) * np.nan

        norm_dataset = scan["measurement/ct24"]
        assert isinstance(norm_dataset, h5py.Dataset)

        norm_data = np.zeros(nFrames, dtype=norm_dataset.dtype)
        norm_data[: len(norm_dataset)] = norm_dataset[()]

        return norm_data

    def merge_scans(self, file_name: str, fillvalue: float = 0):
        raise NotImplementedError()

    def create_average_plots(
        self,
        process: h5py.Group,
        summed_image: np.ndarray,
        summed_integrations: Dict[str, np.ndarray],
        N_frames: int,
    ):
        mean_array = summed_image / N_frames
        mean_image_plot = nx.create_data_plot(process, "image", [], ["data"])
        mean_image_plot["data"] = mean_array
        mean_image_plot["data"].attrs["interpretation"] = "image"

        # Copy to avoid mutating original integration parameters
        integration_params = {**self.integration_params}
        # Mask on the sum to catch the saturated values
        mean_mask = np.logical_or(self.ai.detector.mask, hot_pixel_mask(summed_image))
        integration_params["mask"] = (
            np.logical_or(mean_mask, integration_params["mask"])
            if integration_params.get("mask", None) is not None
            else mean_mask
        )
        integration_of_mean = self.ai._integrate1d_ng(
            mean_array,
            **integration_params,
        )
        default_plot = nx.create_simple_pyFAI_plot(
            process,
            integration_of_mean.radial,
            integration_of_mean.unit.unit_symbol,
            integration_of_mean.intensity,
            "integration_of_average",
        )
        for k, summed_integration in summed_integrations.items():
            nx.create_simple_pyFAI_plot(
                process,
                integration_of_mean.radial,
                integration_of_mean.unit.unit_symbol,
                summed_integration / N_frames,
                f"average_of_{k}",
            )

            # Add normalization

        return default_plot
