import numpy as np
import h5py
import hdf5plugin  # noqa : F401
from typing import List, Tuple
from silx.io.utils import h5py_read_dataset
import re

from .base import BaseIntegrator
from ..io import nexus as nx
from ..utils import extend_linear_dataset, get_scans
from .utils import compute_accumulated_intensity_map


class LoopscanIntegrator(BaseIntegrator):
    valid_scan_titles = ["loopscan", "eiger"]

    def create_pyFAI_plot(
        self,
        pyFAI_process: h5py.Group,
        radial_values: np.ndarray,
        radial_unit: str,
        frame_integrations: np.ndarray,
        scan: h5py.Group,
        plot_name: str,
    ) -> h5py.Group:
        plot = nx.create_data_plot(
            pyFAI_process,
            plot_name,
            ["elapsed_time", "twotheta"],
            ["data"],
        )
        plot["twotheta"] = radial_values
        plot["twotheta"].attrs["units"] = radial_unit
        nx.mark_default_plot(plot, pyFAI_process)

        nFrames = self.get_scan_frames(scan)
        time = scan["measurement/elapsed_time"][:nFrames]
        plot["elapsed_time"] = (
            extend_linear_dataset(time, nFrames) if len(time) < nFrames else time
        )

        plot["data"] = frame_integrations
        plot["data"].attrs["interpretation"] = "spectrum"

        return plot

    def get_scan_size(self, scan: h5py.Group):
        return self.get_scan_frames(scan)

    def get_scan_frames(self, scan: h5py.Group) -> int:
        (nFrames, _) = self.get_scan_params(scan)
        return nFrames

    def get_scan_params(self, scan: h5py.Group) -> Tuple[int, float]:
        scan_title = h5py_read_dataset(scan["title"])
        assert isinstance(scan_title, str)

        if scan_title.startswith(self.valid_scan_titles[0]):
            # scan_title: loopscan nFrames dt
            _, nFrames, dt = scan_title.split()
            return int(nFrames), float(dt)

        if scan_title.startswith(self.valid_scan_titles[1]):
            g = re.search(r"nb_frame: (\d+), expo: ((?:\d|\.)+)", scan_title)
            nFrames, dt = g.groups()
            return int(nFrames), float(dt)

        raise TypeError(f"{scan.name} in {scan.file} is not a loopscan !")

    def merge_scans(
        self,
        file_name: str,
        fillvalue: float = 0,
    ):
        with h5py.File(file_name, "r+") as h5file:
            entry = (
                h5file["merged"]
                if "merged" in h5file
                else nx.create_entry(h5file, "merged")
            )
            assert isinstance(entry, h5py.Group)
            process = nx.create_process(entry, "integrate")

            map_shape, map_slices = get_map_shape_and_slices(file_name)

            scans = get_scans(h5file)

            # Deal first with normalization
            norm_layout = None
            for i, scan in enumerate(scans):
                dset = scan["normalization"]
                if i == 0:
                    norm_layout = h5py.VirtualLayout(
                        shape=(map_shape[0],), dtype=dset.dtype
                    )
                norm_layout[map_slices[i]] = h5py.VirtualSource(
                    ".", dset.name, shape=dset.shape, dtype=dset.dtype
                )
            if norm_layout is not None:
                entry.create_virtual_dataset(
                    "normalization", norm_layout, fillvalue=fillvalue
                )

            for nx_grp_name in self.integration_types:
                plot = nx.create_data_plot(
                    process,
                    nx_grp_name,
                    ["elapsed_time", "twotheta"],
                    ["data"],
                )

                time_layout = None
                map_layout = None
                for i, scan in enumerate(scans):
                    dset = scan[f"integrate/{nx_grp_name}/data"]
                    if i == 0:
                        map_layout = h5py.VirtualLayout(
                            shape=map_shape, dtype=dset.dtype
                        )
                    map_layout[map_slices[i]] = h5py.VirtualSource(
                        ".", dset.name, shape=dset.shape, dtype=dset.dtype
                    )

                    dset = scan[f"integrate/{nx_grp_name}/elapsed_time"]
                    if i == 0:
                        time_layout = h5py.VirtualLayout(
                            shape=(map_shape[0],), dtype=dset.dtype
                        )
                    time_layout[map_slices[i]] = h5py.VirtualSource(
                        ".", dset.name, shape=dset.shape, dtype=dset.dtype
                    )

                    if "twotheta" not in plot:
                        plot["twotheta"] = np.array(
                            scan[f"integrate/{nx_grp_name}/twotheta"]
                        )
                        plot["twotheta"].attrs.update(
                            scan[f"integrate/{nx_grp_name}/twotheta"].attrs
                        )

                if map_layout is not None:
                    plot.create_virtual_dataset("data", map_layout, fillvalue=fillvalue)
                    plot["data"].attrs["interpretation"] = "spectrum"
                if time_layout is not None:
                    plot.create_virtual_dataset(
                        "elapsed_time", time_layout, fillvalue=fillvalue
                    )
                acc_dset = compute_accumulated_intensity_map(process, nx_grp_name)
                acc_dset.attrs["interpretation"] = "spectrum"
                nx.mark_default_plot(plot, process)
                nx.mark_default_plot(plot, entry)

                print(
                    f"Patches merged in {file_name}::merged/integrate/{nx_grp_name} !"
                )


def get_map_shape_and_slices(
    file_name: str,
) -> Tuple[Tuple[int, int], List[slice]]:
    patch_shape = None

    with h5py.File(file_name, "r") as h5file:
        scans = get_scans(h5file)
        n_patches = len(scans)
        for patch in scans:
            patch_shape = patch["integrate/integration_map/data"].shape

    if patch_shape is None:
        raise KeyError(f"Found no patch in {file_name}")

    print(
        f"Map layout in {file_name} was found to be {n_patches} of dimensions {patch_shape}"
    )

    n_frames, Nth = patch_shape

    return (n_patches * n_frames, Nth), [
        slice(i * n_frames, (i + 1) * n_frames) for i in range(n_patches)
    ]
