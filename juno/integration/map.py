import numpy as np
import h5py
import hdf5plugin  # noqa : F401
from typing import Any, Dict, List, Optional, Tuple
from pyFAI.azimuthalIntegrator import AzimuthalIntegrator
from silx.io.utils import h5py_read_dataset

from .base import BaseIntegrator
from ..io import nexus as nx
from ..utils import extend_linear_dataset, get_scans, get_number_unique
from .utils import compute_accumulated_intensity_map


class MapIntegrator(BaseIntegrator):
    valid_scan_titles = ["akmap_lut"]

    def __init__(
        self,
        source_file: str,
        output_file: str,
        ai: AzimuthalIntegrator,
        integration_params: Dict[str, Any],
        separation_params: Optional[Dict[str, Any]],
        dectris_format: bool = False,
        allow_missing_patches: bool = False,
    ) -> None:
        super().__init__(
            source_file,
            output_file,
            ai,
            integration_params,
            separation_params,
            dectris_format,
        )
        self.allow_missing_patches = allow_missing_patches

    def create_pyFAI_plot(
        self,
        pyFAI_process: h5py.Group,
        radial_values: np.ndarray,
        radial_unit: str,
        frame_integrations: np.ndarray,
        scan: h5py.Group,
        plot_name: str,
    ) -> h5py.Group:
        plot = nx.create_data_plot(
            pyFAI_process,
            plot_name,
            ["columns", "rows", "twotheta"],
            ["data"],
        )
        plot["twotheta"] = radial_values
        plot["twotheta"].attrs["units"] = radial_unit
        nx.mark_default_plot(plot, pyFAI_process)

        xPixels, yPixels = self.get_scan_size(scan)

        y_motor_pos = scan["measurement/ustry_position"][:yPixels]
        plot["rows"] = (
            extend_linear_dataset(y_motor_pos, yPixels)
            if len(y_motor_pos) < yPixels
            else y_motor_pos
        )

        x_motor_pos = scan["measurement/ustrz"][::yPixels]
        plot["columns"] = (
            extend_linear_dataset(x_motor_pos, xPixels)
            if len(x_motor_pos) < xPixels
            else x_motor_pos
        )

        plot["data"] = np.reshape(
            frame_integrations, (xPixels, yPixels, frame_integrations.shape[-1])
        )
        plot["data"].attrs["interpretation"] = "spectrum"

        return plot

    def get_scan_frames(self, scan: h5py.Group) -> int:
        scan_size_x, scan_size_y = self.get_scan_size(scan)
        return scan_size_x * scan_size_y

    def get_scan_size(self, scan: h5py.Group) -> Tuple[int, int]:
        scan_title = h5py_read_dataset(scan["title"])
        assert isinstance(scan_title, str)

        if not any(
            [
                scan_title.startswith(valid_scan_title)
                for valid_scan_title in self.valid_scan_titles
            ]
        ):
            raise TypeError(f"{scan.name} in {scan.file} is not a map !")

        # scan_title: akmap_lut( yMotor, yStart, yEnd, yPixels, xMotor, xStart, xEnd, xPixels, dt )
        # map_params = [yMotor, yStart, yEnd, yPixels, xMotor, xStart, xEnd, xPixels, dt]
        map_params = scan_title.split(",")
        yPixels = int(map_params[3])
        xPixels = int(map_params[7])

        return xPixels, yPixels

    def get_normalization(self, scan: h5py.Group) -> np.ndarray:
        norm_data = super().get_normalization(scan)
        scan_size_x, scan_size_y = self.get_scan_size(scan)
        return np.reshape(norm_data, (scan_size_x, scan_size_y))

    def merge_scans(self, file_name: str, fillvalue: float = 0):
        return self.merge_map_scans(
            file_name, self.integration_types, fillvalue, self.allow_missing_patches
        )

    @staticmethod
    def merge_map_scans(
        file_name: str,
        nx_grps_to_merge: List[str],
        fillvalue: float = 0,
        allow_missing_patches: bool = False,
    ):
        with h5py.File(file_name, "r+") as h5file:
            entry = (
                h5file["merged"]
                if "merged" in h5file
                else nx.create_entry(h5file, "merged")
            )
            assert isinstance(entry, h5py.Group)
            process = nx.create_process(entry, "integrate")

            # The map shape can be deduced from any NX group to merge
            map_shape, map_slices = get_map_shape_and_slices(
                file_name, nx_grps_to_merge[0], allow_missing_patches
            )
            scans = get_scans(h5file)

            # Deal first with normalization
            norm_layout = None
            for i, scan in enumerate(scans):
                dset = scan["normalization"]
                if i == 0:
                    norm_layout = h5py.VirtualLayout(
                        shape=(map_shape[0], map_shape[1]), dtype=dset.dtype
                    )
                norm_layout[map_slices[i]] = h5py.VirtualSource(
                    ".", dset.name, shape=dset.shape, dtype=dset.dtype
                )
            if norm_layout is not None:
                entry.create_virtual_dataset(
                    "normalization", norm_layout, fillvalue=fillvalue
                )

            for nx_grp_name in nx_grps_to_merge:
                plot = nx.create_data_plot(
                    process,
                    nx_grp_name,
                    ["columns", "rows", "twotheta"],
                    ["data"],
                )

                map_layout = None
                columns_layout = None
                rows_layout = None
                for i, scan in enumerate(scans):
                    dset = scan[f"integrate/{nx_grp_name}/data"]
                    if i == 0:
                        map_layout = h5py.VirtualLayout(
                            shape=map_shape, dtype=dset.dtype
                        )
                    map_layout[map_slices[i]] = h5py.VirtualSource(
                        ".", dset.name, shape=dset.shape, dtype=dset.dtype
                    )

                    dset = scan[f"integrate/{nx_grp_name}/columns"]
                    if i == 0:
                        columns_layout = h5py.VirtualLayout(
                            shape=(map_shape[0],), dtype=dset.dtype
                        )
                    columns_layout[map_slices[i][0]] = h5py.VirtualSource(
                        ".", dset.name, shape=dset.shape, dtype=dset.dtype
                    )

                    dset = scan[f"integrate/{nx_grp_name}/rows"]
                    if i == 0:
                        rows_layout = h5py.VirtualLayout(
                            shape=(map_shape[1],), dtype=dset.dtype
                        )
                    rows_layout[map_slices[i][1]] = h5py.VirtualSource(
                        ".", dset.name, shape=dset.shape, dtype=dset.dtype
                    )

                    if "twotheta" not in plot:
                        plot["twotheta"] = np.array(
                            scan[f"integrate/{nx_grp_name}/twotheta"]
                        )
                        plot["twotheta"].attrs.update(
                            scan[f"integrate/{nx_grp_name}/twotheta"].attrs
                        )

                if map_layout is not None:
                    plot.create_virtual_dataset("data", map_layout, fillvalue=fillvalue)
                    plot["data"].attrs["interpretation"] = "spectrum"
                if columns_layout is not None:
                    plot.create_virtual_dataset(
                        "columns", columns_layout, fillvalue=fillvalue
                    )
                if rows_layout is not None:
                    plot.create_virtual_dataset(
                        "rows", rows_layout, fillvalue=fillvalue
                    )

                nx.mark_default_plot(plot, process)
                nx.mark_default_plot(plot, entry)

                acc_dset = compute_accumulated_intensity_map(process, nx_grp_name)
                acc_dset.attrs["interpretation"] = "image"

                print(
                    f"Patches merged in {file_name}::merged/integrate/{nx_grp_name} !"
                )


def get_map_shape_and_slices(
    file_name: str,
    integration_process_name: str,
    allow_missing_patches: bool = False,
) -> Tuple[Tuple[int, int, int], List[Tuple[slice, slice]]]:
    Y_motor_pos = []
    X_motor_pos = []
    patch_shape = None

    with h5py.File(file_name, "r") as h5file:
        scans = get_scans(h5file)
        n_patches = len(scans)
        for patch in scans:
            integration_process = patch[f"integrate/{integration_process_name}"]
            assert isinstance(integration_process, h5py.Group)
            patch_shape = integration_process["data"].shape
            Y_motor_pos.append(integration_process["rows"][0])
            X_motor_pos.append(integration_process["columns"][0])

    if patch_shape is None:
        raise KeyError(f"Found no patch in {file_name}")

    patch_numbers = (get_number_unique(X_motor_pos), get_number_unique(Y_motor_pos))

    if not allow_missing_patches and patch_numbers[0] * patch_numbers[1] != n_patches:
        raise ValueError(
            f"Found {patch_numbers} patches from motor positions while the file contains {n_patches} patches !"
        )

    print(
        f"Map layout in {file_name} was found to be {patch_numbers} of dimensions {patch_shape}"
    )

    X, Y, Nth = patch_shape

    return (patch_numbers[0] * X, patch_numbers[1] * Y, Nth), [
        (slice(i * X, (i + 1) * X), slice(j * Y, (j + 1) * Y))
        for i in range(patch_numbers[0])
        for j in range(patch_numbers[1])
    ]
