from typing import Dict
import h5py
import numpy as np
import os

from ..io import nexus as nx

Integrations = Dict[str, np.ndarray]


def compute_accumulated_intensity_map(
    pyFAI_process: h5py.Group, gname: str
) -> h5py.Dataset:
    integ_map_data = pyFAI_process[f"{gname}/data"]
    assert isinstance(integ_map_data, h5py.Dataset)

    acc_map_plot = nx.create_data_plot(
        pyFAI_process, f"accumulated_intensity_of_{gname}", [], ["data"]
    )
    acc_dataset = acc_map_plot.create_dataset(
        "data", data=np.sum(integ_map_data, axis=-1)
    )
    return acc_dataset


def generate_dectris_frame_iterator(scan_dataset: h5py.Dataset):
    (_, file_name, __, ___) = scan_dataset.virtual_sources()[0]
    n_frames = len(scan_dataset)

    path_members = file_name.split("/")[:2]
    (basename, ext) = os.path.splitext(os.path.basename(scan_dataset.file.filename))
    path_to_the_actual_file = os.path.join(
        os.path.dirname(scan_dataset.file.filename),
        *path_members,
        f"{basename}_00001_master.h5",
    )

    n_yielded_frames = 0
    with h5py.File(path_to_the_actual_file, "r") as h5file:
        data_group = h5file["entry/data"]
        for dset_name in sorted(list(data_group)):
            for frame in data_group[dset_name]:
                yield frame
                n_yielded_frames += 1
                if n_yielded_frames >= n_frames:
                    return
