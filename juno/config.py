from enum import Flag
from typing import List, Union, Type


def flag_from_config(values: Union[List[str], str], enum: Type[Flag]) -> Flag:
    if isinstance(values, str):
        values = [values]
    result = None
    for value in values:
        if result is None:
            result = enum.__members__[value]
        else:
            result |= enum.__members__[value]
    return result
