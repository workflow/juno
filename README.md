# IMPORTANT NOTICE

This project is archived: it is still in a working state but no further development/bug fixes will be done.

Azimuthal integration can be performed using Ewoks workflows using [ewoksxrpd](https://gitlab.esrf.fr/workflow/ewoksapps/ewoksxrpd).

# Juno: Jupyter notebooks for azimuthal integration

This project contains notebooks to create integration maps from data acquired at ID13.

## Installation

This project is aimed to be used on [jupyter-slurm](https://jupyter-slurm.esrf.fr).

Connect to a session and request the a node on the `jupyter-p9gpu` partition **with a GPU** and select the **October 2023** Jupyter environment.

![Jupyter-slurm page](https://gitlab.esrf.fr/workflow/juno/uploads/6c11b2186ab2a9c94c052f447733befd/image.png)

Then, open a terminal using one of the two methods

- _In Classic Jupyter_
  ![Classic Jupyter terminal opening](https://gitlab.esrf.fr/workflow/juno/uploads/8da99287c757f47e9db1a447018e3868/image.png)
- _In JupyterLab_
  ![JupyterLab terminal opening](https://gitlab.esrf.fr/workflow/juno/uploads/2a209abfcf6c78816bd361ea0182915c/image.png)

Finally, in this terminal, run

```
git clone https://gitlab.esrf.fr/workflow/juno
```

This creates a `juno` folder containing the notebooks and all the project files.

No additional installation steps should be needed, you can navigate in the `juno` folder in Jupyter to open the introduction notebook **Using_notebooks.ipynb**.

## Usage

See [Using_notebooks.ipynb](./Using_notebooks.ipynb)

## Troubleshooting

If you encounter a problem, open an issue on [Gitlab](https://gitlab.esrf.fr/workflow/juno/-/issues).

## Development

See [CONTRIBUTING.md](./CONTRIBUTING.md)
