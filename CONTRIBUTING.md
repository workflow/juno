## Set up

Requirements are listed in `setup.cfg` and can be installed with

```bash
pip install [--user] .[dev]
```

## Linting

The configuration is in `setup.cfg`.

### Python files

- Formatted with [black](https://github.com/psf/black)
- Linted with [flake8](https://github.com/PyCQA/flake8)

### Jupyter notebooks

- Formatted with [black](https://github.com/psf/black) (support of Jupyter notebooks was added in [21.9b0](https://black.readthedocs.io/en/stable/change_log.html))
- Linted with [flake8-nb](https://github.com/s-weigand/flake8-nb)

**Formatting is checked by the linting so be sure to format files before committing.**

## Testing

Tests are in `test_notebooks.py` and make use [pytest](https://docs.pytest.org/en/stable/index.html) and [testbook](https://testbook.readthedocs.io/en/latest/). They follow the following procedure:

- Create `run_config.yml` from a test config to generate files in a temporary directory
- For each notebook:
  - Generate the needed input files
  - Run the notebook
  - Do some additional tests
  - Remove the input files

## Releasing

The versioning is handled by [bump2version](https://github.com/c4urself/bump2version). To release a new version, run:

```sh
bumpversion [patch|minor|major]
```

This will bump the version in `juno/VERSION`, commit and tag with the new version number. Then, you can push the bumping version commit and tag:

```sh
git push && git push --tags
```

Finally, write release notes in the [Tags](https://gitlab.esrf.fr/workflow/juno/-/tags) page of the repo.
